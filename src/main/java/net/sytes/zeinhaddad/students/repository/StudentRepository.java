package net.sytes.zeinhaddad.students.repository;

import java.util.Optional;
import net.sytes.zeinhaddad.students.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface untuk meng-generate Data Access Object (DAO).
 * @author zein
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByLastName(String lastName);
}
