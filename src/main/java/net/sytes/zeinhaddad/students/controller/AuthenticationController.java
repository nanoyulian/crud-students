package net.sytes.zeinhaddad.students.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller yang mengatur halaman dan proses autentikasi.
 * @author zein
 */
@Controller
public class AuthenticationController {
    /**
     * Halaman login.
     * @return 
     */
    @GetMapping("/login")
    public String showLogin() {
        return "/login";
    }
}
