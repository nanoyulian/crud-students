package net.sytes.zeinhaddad.students.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

/**
 * Kelas untuk mengatur konfigurasi Spring Security.
 * @author zein
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    @Value("${spring.security.user.name}")
    private String adminUsername;

    @Value("${spring.security.user.password}")
    private String adminPassword;

    /**
     * Mengatur filter chain di spring security.
     * halaman '/' boleh diakses semua orang.
     * halaman '/students' boleh diakses admin atau user.
     * halaman '/admin/students' hanya boleh diakses admin.
     * 
     * @param http
     * @return
     * @throws Exception 
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((requests) -> requests
                    .requestMatchers("/").permitAll()
                    .requestMatchers("/students").hasAnyRole("ADMIN", "USER")
                    .anyRequest().hasRole("ADMIN")
                    //.requestMatchers("/admin/students/**").hasRole("ADMIN")
                    //.anyRequest().authenticated()
                )
                .formLogin((form) -> form
                    .loginPage("/login")
                    .permitAll()
                )
                .logout((logout) -> logout.permitAll());

        return http.build();
    }

    /**
     * Membuat user di in memory database.
     * Hanya untuk testing.
     * 
     * @return 
     */
    @Bean
    public UserDetailsService userDetailsService() {
        List<UserDetails> users = new ArrayList<>();
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user")
                .password("pass")
                .roles("USER")
                .build();

        users.add(user);
        users.add(User.withDefaultPasswordEncoder()
                .username(adminUsername)
                .password(adminPassword)
                .roles("ADMIN")
                .build()
        );

        return new InMemoryUserDetailsManager(users);
    }
}
